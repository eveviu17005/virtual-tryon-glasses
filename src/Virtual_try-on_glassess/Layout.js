import React, { Component } from "react";
import Header from "./Header";
import Body from "./Body";
import Footer from "./Footer";
import styles from "./index.module.css";
import { data } from "./dataJson";
import ItemGlass from "./ItemGlass";

export default class Layout extends Component {
  state = {
    glassesArr: data,
    choosenGlass: "",
  };

  handleChooseGlass = (item) => {
    this.setState({ choosenGlass: item });
  };

  render() {
    return (
      <div className={styles.background}>
        <Header />
        <Body choosenGlass={this.state.choosenGlass} />
        <Footer
          handleChooseGlass={this.handleChooseGlass}
          dataShoe={this.state.glassesArr}
        />
      </div>
    );
  }
}
