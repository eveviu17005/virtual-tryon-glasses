import React, { Component } from "react";
import styles from "./index.module.css";
export default class ItemGlass extends Component {
  render() {
    let { url } = this.props.data;
    return (
      <div className="col-4 w-100">
        <img
          className={`${styles.itemGlass}`}
          onClick={() => {
            this.props.handleChooseGlass(this.props.data);
          }}
          src={url}
          alt=""
        />
      </div>
    );
  }
}
