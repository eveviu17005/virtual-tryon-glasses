import React, { Component } from "react";
import ItemGlass from "./ItemGlass";
import styles from "./index.module.css";

export default class Footer extends Component {
  renderItemShoe = () => {
    return this.props.dataShoe.map((item) => {
      return (
        <ItemGlass
          handleChooseGlass={this.props.handleChooseGlass}
          data={item}
        />
      );
    });
  };

  render() {
    return (
      <div className={`${styles.itemGalesses} row mt-5`}>
        {this.renderItemShoe()}
      </div>
    );
  }
}
