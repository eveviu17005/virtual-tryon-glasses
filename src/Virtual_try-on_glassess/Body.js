import React, { Component } from "react";
import styles from "./index.module.css";
export default class Body extends Component {
  render() {
    let { url, name, price, desc } = this.props.choosenGlass;
    return (
      <>
        <div className={`${styles.glassesModel}`}>
          <div className={`${styles.glassChoosen}`}>
            <img src={this.props.choosenGlass.url} alt="" />
          </div>
          <div className={styles.glassesInfo}>
            <h5>{name}</h5>
            <h2>$ {price}</h2>
            <p>{desc}</p>
          </div>
        </div>
      </>
    );
  }
}
