import React, { Component } from "react";
import styles from "./index.module.css";

export default class Header extends Component {
  render() {
    return (
      <header className={styles.header_title}>TRY GLASSES APP ONLINE</header>
    );
  }
}
