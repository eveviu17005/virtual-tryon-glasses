import logo from "./logo.svg";
import "./App.css";
import Layout from "./Virtual_try-on_glassess/Layout";

function App() {
  return (
    <div className="App">
      <Layout />
    </div>
  );
}

export default App;
